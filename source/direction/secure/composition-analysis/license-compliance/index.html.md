---
layout: markdown_page
title: "Category Vision - License Compliance"
---

- TOC
{:toc}

## Description

### Overview

License Compliance analyses your application to track which licenses are used by third-party components, like libraries and external dependencies, and check that they are compatible with the licensing model.

Licenses can be incompatible with the chosen license model for the application, for example because of their redistribution rights.

### Goal

Our goal is to provide License Compliance as part of the standard development process. This means that License Compliance is executed every time a new commit is pushed to a branch. We also include License Compliance as part of [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

Maintainers can define the set of approved and blacklisted licenses for their application, so developers can validate their changes against the existing policy.

License Compliance results can be consumed in the merge request, where only users can see which new license is introduced by the new code, and which are the libraries that are licensed in that way. A full report is available in the pipeline details page.

Licenses should also be included in a bill of materials (BOM), where all the components are listed with their licenses. See [this issue](https://gitlab.com/gitlab-org/gitlab-ee/issues/7476) for additional details.

### Roadmap

- [First MVC (already shipped)](https://docs.gitlab.com/ee/user/application_security/license_management/index.html)
- [License Compliance improvements](https://gitlab.com/groups/gitlab-org/-/epics/273)

## What's Next & Why

License policies are often shared between multiple projects in the same group/organization. That's why it is important to share the allowed/blacklisted policies for all the projects in the same group.

The next MVC is to [implement group-level license compliance](https://gitlab.com/gitlab-org/gitlab-ee/issues/7149).

## Maturity Plan
 - [Base Epic](https://gitlab.com/groups/gitlab-org/-/epics/531)

## Competitive Landscape

- [Black Duck](https://www.blackducksoftware.com/solutions/open-source-license-compliance)
- [Sonatype Nexus](https://www.sonatype.com/nexus-auditor)
- [Whitesource](https://www.whitesourcesoftware.com/open-source-license-compliance/)

## Analyst Landscape

The License Compliance topic is often coupled with Dependency Scanning in Software Composition Analysis (SCA). This is what analysts evaluate, and how it is bundled in other products. As defined in our [Solutions](https://about.gitlab.com/handbook/product/categories/index.html#solutions), GitLab includes Container Scanning as part of Software Composition Analysis.

We should make sure that we can address the entire solution even if we consider these features as independent, and to leverage the single application nature of GitLab to provide a consistent experience in all of them.

- https://www.forrester.com/report/The+Forrester+Wave+Software+Composition+Analysis+Q1+2017/-/E-RES136463

## Top Customer Success/Sales Issue(s)

- [License Compliance should skip maven tests](https://gitlab.com/gitlab-org/gitlab-ee/issues/6838)

[Full list](https://gitlab.com/groups/gitlab-org/-/issues?state=opened&sort=milestone&label_name%5B%5D=customer&label_name%5B%5D=license%20compliance)

## Top user issue(s)

- [License Compliance should skip maven tests](https://gitlab.com/gitlab-org/gitlab-ee/issues/6838)

[Full list](https://gitlab.com/groups/gitlab-org/-/issues?state=opened&sort=popularity&label_name%5B%5D=license%20compliance)

## Top internal customer issue(s)

- [Issue 1](https://gitlab.com/gitlab-org/gitlab-ee/issues/8543)

## Top Vision Item(s)

- [Issue 1](https://gitlab.com/gitlab-org/gitlab-ee/issues/7149)
- [Issue 2](https://gitlab.com/gitlab-org/gitlab-ee/issues/6924)
- [Issue 3](https://gitlab.com/gitlab-org/gitlab-ee/issues/7476)
