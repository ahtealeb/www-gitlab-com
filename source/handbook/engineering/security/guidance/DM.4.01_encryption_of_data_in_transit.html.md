---
layout: markdown_page
title: "DM.4.01 - Encryption of Data in Transit Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# DM.4.01 - Encryption of Data in Transit

## Control Statement

GitLab encrypts red, orange, and yellow data transmitted over public networks.

## Context

Encrypting data transmitted over public networks helps ensure the confidentiality and integrity of that data. Without encryption, data in transit over public networks can easily be intercepted using automated, open source tools and viewed and maliciously modified by malicious actors.

## Scope

Encrypting data in transit over public networks applies to all red, orange, and yellow data.

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLab team-members, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/DM.4.01_encryption_of_data_in_transit.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/DM.4.01_encryption_of_data_in_transit.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/DM.4.01_encryption_of_data_in_transit.md).

## Framework Mapping

* ISO
  * A.13.2.3
  * A.14.1.2
  * A.14.1.3
  * A.18.1.4
  * A.18.1.5
* SOC2 CC
  * CC6.7
* PCI
  * 2.3
  * 4.1
  * 4.1.1
  * 8.2.1
